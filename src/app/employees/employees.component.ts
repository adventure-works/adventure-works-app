import { Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {EmployeesService} from "../services/employees.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.sass']
})
export class EmployeesComponent implements OnInit, AfterViewInit {

  dataSource!: MatTableDataSource<any>;
  @ViewChild('paginator') paginator! : MatPaginator;
  @ViewChild(MatSort) matSort! : MatSort;

  displayedColumns: string[] = [
    'EmployeeID',
    'NationalIDNumber',
    'ContactID',
    'LoginID',
    'ManagerID',
    'Title',
    'BirthDate',
    'MaritalStatus',
    'Gender',
    'HireDate',
    'SalariedFlag',
    'VacationHours',
    'SickLeaveHours',
    'CurrentFlag',
    'ModifiedDate',
  ];

  constructor(private EmployeesService: EmployeesService) {}

  ngOnInit(): void {

    this.EmployeesService.getAllEmployees().subscribe((res: any) => {
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.matSort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'BirthDate': return new Date(item.BirthDate);
          case 'HireDate': return new Date(item.HireDate);
          case 'ModifiedDate': return new Date(item.ModifiedDate);
          default: return item[property];
        }
      };
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {}
}
