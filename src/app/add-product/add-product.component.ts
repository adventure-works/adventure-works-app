import { Component, OnInit } from '@angular/core';
import {Products} from "../models/Products";
import {ProductService} from "../services/product.service";
import {SubcategoriesService} from "../services/subcategories.service";
import {Subcategory} from "../models/Subcategory";
import { NotifierService } from 'angular-notifier';
import {Model} from "../models/Models";
import {ModelsService} from "../services/models.service";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.sass']
})
export class AddProductComponent implements OnInit {
  private readonly notifier: NotifierService;

  generateGuid() : string {
    return 'xxxxxxxxx4xxxyxx'.replace(/[xy]/g, function(c) {
      let r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  newDate = new Date();

  product: Products = {
    ProductID: 0,
    Name: "",
    ProductNumber: "",
    MakeFlag: 0,
    FinishedGoodsFlag: 0,
    Color: "",
    SafetyStockLevel: 0,
    ReorderPoint: 0,
    StandardCost: 0,
    ListPrice: 0,
    Size: "",
    SizeUnitMeasureCode: "",
    WeightUnitMeasureCode: "",
    Weight: 0,
    DaysToManufacture: 0,
    ProductLine: "",
    Class: "",
    Style: "",
    ProductSubcategoryID: null,
    ProductModelID: null,
    SellStartDate: this.newDate.toISOString().split('T')[0],
    SellEndDate: this.newDate.toISOString().split('T')[0],
    DiscontinuedDate: this.newDate.toISOString().split('T')[0],
    ModifiedDate: this.newDate.toISOString().split('T')[0],
    rowguid: this.generateGuid(),
  };
  submitted = false;

  subcategories?: Subcategory[];
  models?: Model[];

  constructor(private ProductService: ProductService, private SubcategoriesService: SubcategoriesService,
              private modelService: ModelsService, notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    this.getAllSubcategories();
    this.getAllModels();
  }

  getAllSubcategories(): void {
    this.SubcategoriesService.getAllSubcategories().subscribe((subcategories) => {
      console.log(subcategories);
      this.subcategories = subcategories;
    })
  }

  getAllModels(): void {
    this.modelService.getAllModels().subscribe((models) => {
      this.models = models;
    })
  }


  createNewProduct(): void {
    const data = {
      Name: this.product.Name,
      ProductNumber: this.product.ProductNumber,
      MakeFlag: this.product.MakeFlag,
      FinishedGoodsFlag: this.product.FinishedGoodsFlag,
      Color: this.product.Color,
      SafetyStockLevel: this.product.SafetyStockLevel,
      ReorderPoint: this.product.ReorderPoint,
      StandardCost: this.product.StandardCost,
      ListPrice: this.product.ListPrice,
      Size: this.product.Size,
      SizeUnitMeasureCode: this.product.SizeUnitMeasureCode,
      WeightUnitMeasureCode: this.product.WeightUnitMeasureCode,
      Weight: this.product.Weight,
      DaysToManufacture: this.product.DaysToManufacture,
      ProductLine: this.product.ProductLine,
      Class: this.product.Class,
      Style: this.product.Style,
      ProductSubcategoryID: this.product.ProductSubcategoryID,
      ProductModelID: this.product.ProductModelID,
      SellStartDate: this.product.SellStartDate,
      SellEndDate: this.product.SellEndDate,
      DiscontinuedDate: this.product.DiscontinuedDate,
      ModifiedDate: this.product.ModifiedDate,
      rowguid: this.product.rowguid,
    };

    this.ProductService.createProduct(data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.notifier.notify('success', 'Product Added !');
          this.submitted = true;
        },
        error: (e) => {
          console.error(e);
          this.notifier.notify('error', 'An error occurred while adding product');
        }
      });
  }
}
