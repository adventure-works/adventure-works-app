import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductsListComponent} from "./products-list/products-list.component";
import {EmployeesComponent} from "./employees/employees.component";
import {AddProductComponent} from "./add-product/add-product.component";

const routes: Routes = [
  { path: 'products', component: ProductsListComponent },
  { path: 'products/create', component: AddProductComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: '**', redirectTo: 'products' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
