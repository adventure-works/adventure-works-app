import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Employees } from "../models/Employees";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})

export class EmployeesService {
  baseUrl = 'http://localhost:3000/employees';
  httpsOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  public getAllEmployees(): Observable<Employees[]> {
    return this.http.get<Employees[]>(this.baseUrl + '/', this.httpsOptions);
  }
}
