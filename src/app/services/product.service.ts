import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Products } from "../models/Products";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import {Subcategory} from "../models/Subcategory";

@Injectable({
  providedIn: "root"
})

export class ProductService {
  baseUrl = 'http://localhost:3000/products';
  httpsOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  createProduct(Product: { MakeFlag: number; Size: string; SizeUnitMeasureCode: string; Color: string; SafetyStockLevel: number; ProductNumber: string; ListPrice: number; ModifiedDate: string; Weight: number; rowguid: string; Name: string; FinishedGoodsFlag: number; ReorderPoint: number; DaysToManufacture: number; SellStartDate: string; SellEndDate: string; Style: string; DiscontinuedDate: string; StandardCost: number; Class: string; ProductSubcategoryID: number | null; ProductModelID: number | null; ProductLine: string; WeightUnitMeasureCode: string }): Observable<Products> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };
    return this.http.post<Products>(this.baseUrl + '/create',
      Product, httpOptions);
  }

  public getAllProducts(): Observable<Products[]> {
    return this.http.get<Products[]>(this.baseUrl + '/', this.httpsOptions);
  }
}
