import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Model} from "../models/Models";

@Injectable({
  providedIn: 'root'
})

export class ModelsService {
  baseUrl = 'http://localhost:3000/models';
  httpsOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  public getAllModels(): Observable<Model[]> {
    return this.http.get<Model[]>(this.baseUrl + '/', this.httpsOptions);
  }

}
