import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ProductService} from "../services/product.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Products} from "../models/Products";

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.sass']
})

export class ProductsListComponent implements OnInit,AfterViewInit {

  dataSource!: MatTableDataSource<any>;
  @ViewChild('paginator') paginator! : MatPaginator;
  @ViewChild(MatSort) matSort! : MatSort;
  displayedColumns: string[] = [
    'ProductID',
    'Name',
    'ProductNumber',
    'MakeFlag',
    'FinishedGoodsFlag',
    'Color',
    'SafetyStockLevel',
    'StandardCost',
    'ListPrice',
    'Size',
    'SizeUnitMeasureCode',
    'WeightUnitMeasureCode',
    'Weight',
    'ProductSubcategoryID',
    'ProductModelID',
  ];

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getAllProducts().subscribe((products: Products[]) => {
      this.dataSource = new MatTableDataSource<Products>(products);
      console.log(products);
      this.dataSource.sort = this.matSort || null;
      this.dataSource.paginator = this.paginator || null;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {}
}
