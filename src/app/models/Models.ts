export class Model {
  ProductModelID: number;
  Name: string;
  CatalogDescription: string;
  Instructions: string;
  ModifiedDate: Date;

  constructor(ProductModelID: number, Name: string, CatalogDescription: string, Instructions: string, ModifiedDate: Date) {
    this.ProductModelID = ProductModelID === undefined ? 0 : ProductModelID;
    this.Name = Name === undefined ? '' : Name;
    this.CatalogDescription = CatalogDescription === undefined ? '' : CatalogDescription;
    this.Instructions = Instructions === undefined ? '' : Instructions;
    this.ModifiedDate = ModifiedDate === undefined ? new Date() : ModifiedDate;
  }
}
