import {SalariedFlag} from "./SalariedFlag";
import {CurrentFlag} from "./CurrentFlag";

export class Employees {
  EmployeeID: number;
  NationalIDNumber: string;
  ContactID: number;
  LoginID: string;
  ManagerID: number;
  Title: string;
  BirthDate: Date;
  MaritalStatus: string;
  Gender: number;
  HireDate: Date;
  SalariedFlag: SalariedFlag;
  VacationHours: number;
  SickLeaveHours: number;
  CurrentFlag: CurrentFlag;
  ModifiedDate: Date;



  constructor(EmployeeID: number, NationalIDNumber: string, ContactID: number, LoginID: string,
              ManagerID: number, Title: string, BirthDate: Date, MaritalStatus: string,
              Gender: number, HireDate: Date, SalariedFlag: SalariedFlag, VacationHours: number, SickLeaveHours: number,
              CurrentFlag: CurrentFlag, ModifiedDate: Date) {
    this.EmployeeID = EmployeeID === undefined ? 0 : EmployeeID;
    this.NationalIDNumber = NationalIDNumber === undefined ? 'N/A' : NationalIDNumber;
    this.ContactID = ContactID === undefined ? 0 : ContactID;
    this.LoginID = LoginID === null ? 'N/A' : LoginID;
    this.ManagerID = ManagerID === undefined ? 0 : ManagerID;
    this.Title = Title === null ? 'N/A' : Title;
    this.BirthDate = BirthDate === undefined ? new Date() : BirthDate;
    this.MaritalStatus = MaritalStatus === null ? 'N' : MaritalStatus;
    this.Gender = Gender === undefined ? 0 : Gender;
    this.HireDate = HireDate === undefined ? new Date() : HireDate;
    this.SalariedFlag = SalariedFlag;
    this.VacationHours = VacationHours === null ? 0 : VacationHours;
    this.SickLeaveHours = SickLeaveHours === null ? 0 : SickLeaveHours;
    this.CurrentFlag = CurrentFlag;
    this.ModifiedDate = ModifiedDate === undefined ? new Date() : ModifiedDate;
  }
}
