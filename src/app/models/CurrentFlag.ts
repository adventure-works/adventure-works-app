export class CurrentFlag {
  type: string;
  data: Array<number>;

  constructor(type: string, data: Array<number>) {
    this.type = type === undefined ? 'N/A' : type;
    this.data = data === undefined ? [] : data;
  }
}
