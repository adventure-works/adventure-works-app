export class SalariedFlag {
  type: string | boolean;
  data: Array<number>;

  constructor(type: boolean, data: Array<number>) {
    this.type = type === undefined ? 'N/A' : type;
    this.data = data === undefined ? [] : data;
  }
}
