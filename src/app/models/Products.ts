import {MakeFlag} from "./MakeFlag";
import {FinishedGoodsFlag} from "./FinishedGoodsFlag";

export class Products {
  ProductID: number;
  Name: string;
  ProductNumber: string;
  MakeFlag: number;
  FinishedGoodsFlag: number;
  Color: string;
  SafetyStockLevel: number;
  ReorderPoint: number;
  StandardCost: number;
  ListPrice: number;
  Size: string;
  SizeUnitMeasureCode: string;
  WeightUnitMeasureCode: string;
  Weight: number;
  DaysToManufacture: number;
  ProductLine: string;
  Class: string;
  Style: string;
  ProductSubcategoryID: null | number;
  ProductModelID: null | number;
  SellStartDate: string;
  SellEndDate: string;
  DiscontinuedDate: string;
  ModifiedDate: string;
  rowguid: string;

  constructor(ProductID: number, Name: string, ProductNumber: string, MakeFlag: number, FinishedGoodsFlag: number, Color: string, SafetyStockLevel: number, ReorderPoint: number, StandardCost: number, ListPrice: number, Size: string, SizeUnitMeasureCode: string, WeightUnitMeasureCode: string, Weight: number, DaysToManufacture: number, ProductLine: string, Class: string, Style: string, ProductSubcategoryID: null | number, ProductModelID: null | number, SellStartDate: string, SellEndDate: string, DiscontinuedDate: string, ModifiedDate: string, rowguid: string) {
    this.ProductID = ProductID === undefined ? 0 : ProductID;
    this.Name = Name === undefined ? 'N/A' : Name;
    this.ProductNumber = ProductNumber === undefined ? 'N/A' : ProductNumber;
    this.MakeFlag = MakeFlag;
    this.FinishedGoodsFlag = FinishedGoodsFlag;
    this.Color = Color === null ? 'N/A' : Color;
    this.SafetyStockLevel = SafetyStockLevel === undefined ? 0 : SafetyStockLevel;
    this.ReorderPoint = ReorderPoint === undefined ? 0 : ReorderPoint;
    this.StandardCost = StandardCost === undefined ? 0 : StandardCost;
    this.ListPrice = ListPrice === undefined ? 0 : ListPrice;
    this.Size = Size === null ? 'N/A' : Size;
    this.SizeUnitMeasureCode = SizeUnitMeasureCode === null ? 'N/A' : SizeUnitMeasureCode;
    this.WeightUnitMeasureCode = WeightUnitMeasureCode === null ? 'N/A' : WeightUnitMeasureCode;
    this.Weight = Weight === null ? 0 : Weight;
    this.DaysToManufacture = DaysToManufacture === undefined ? 0 : DaysToManufacture;
    this.ProductLine = ProductLine === null ? 'N/A' : ProductLine;
    this.Class = Class === null ? 'N/A' : Class;
    this.Style = Style === null ? 'N/A' : Style;
    this.ProductSubcategoryID = ProductSubcategoryID === null ? 0 : ProductSubcategoryID;
    this.ProductModelID = ProductModelID === null ? 0 : ProductModelID;
    this.SellStartDate = SellStartDate;
    this.SellEndDate = SellEndDate;
    this.DiscontinuedDate = DiscontinuedDate;
    this.ModifiedDate = ModifiedDate;
    this.rowguid = rowguid;
  }
}
