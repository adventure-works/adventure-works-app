export class Subcategory {
  ProductSubcategoryID: number;
  ProductCategoryID: number;
  Name: string;
  ModifiedDate: Date;

  constructor(ProductSubcategoryID: number, ProductCategoryID: number, Name: string, ModifiedDate: Date) {
    this.ProductSubcategoryID = ProductSubcategoryID === undefined ? 0 : ProductSubcategoryID;
    this.ProductCategoryID = ProductCategoryID === undefined ? 0 : ProductCategoryID;
    this.Name = Name === undefined ? '' : Name;
    this.ModifiedDate = ModifiedDate === undefined ? new Date() : ModifiedDate;
  }
}
