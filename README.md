# Projet rattrapage - AdventureWorks
## A propos du projet

Cette stack gère l'API afin de pouvoir requêter sur la base de données via le network.

### Getting Started
Voici les différentes étapes listées ci-dessous pour pouvoir obtenir une copie locale opérationnelle.

### Prérequis
* **[Docker-Compose](https://docs.docker.com/compose/install/)**

### Technologies utilisés
* **[Angular 13](https://angular.io/)**

### Installation

#### Installation et lancement du conteneur via un *terminal*.

+ Clonage du projet
````bash
git clone https://gitlab.com/adventure-works/adventure-works-app.git
````

+ Se déplacer dans le projet
````bash
cd adventure-works-app
````

+ Lancement du conteneur

````bash
docker-compose up -d
````

### Utilisation

:warning: Après le lancement du conteneur, la première compilation est très longue et restera en suspend environ 30 s à 2 min sur la console du conteneur via docker desktop comme le montre l'écran suivant :

![Compilation_En_Attente](https://i.imgur.com/735Y2LQ.png)

Lorsque cela sera terminé, le message suivant s'affichera :

![Compilation_Finie](https://i.imgur.com/1Ks0LDw.png)

:warning:

*  Pour pouvoir interagir avec l'application, un lien est disponible ici **[http://localhost:4200/](http://localhost:4200/)**.
